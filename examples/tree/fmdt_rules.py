"""
Generate a set of fuzzy rules for a dataset.
"""
from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np

from FuzzyML.datasets import load_iris
from FuzzyML.fmdt import fmdt

# Dataset loading
X, y = load_iris.load_iris()
n, m = X.shape
np.random.seed(0)
index_tr = np.random.choice(range(n), size=int(n * 0.7), replace=False)
index_te = np.setdiff1d(np.arange(0, n, dtype=int), index_tr)

Xtr = X[index_tr, :]
ytr = y[index_tr]
Xte = X[index_te, :]
yte = y[index_te]

# Inducing the tree
fTree = fmdt.FMDT(minNumExamples=5, discr_threshold=0, max_depth=2, max_prop=1., minGain=0.0001,
                  priorDiscretization=True, verbose=True).fit(Xtr, ytr, [True] * Xtr.shape[1])
print(fTree.numLeaves())

print("Training accuracy: ",sum(fTree.predict(Xtr)==ytr)/float(len(ytr)))
print("Test accuracy: ",sum(fTree.predict(Xte)==yte)/float(len(yte)))

myArr =[]
fTree.tree._ruleMine("",ruleArray=myArr)
print()
for rule in myArr:
    print(rule)

plt.figure()
numFeat = 4
rows = 2
cols = 2
splits = fTree.cPoints

for subFeat in range(1, numFeat + 1):
    plt.subplot(rows, cols, subFeat)
    splits_points = splits[subFeat-1]
    if len(splits_points) != 0:
        fset1 = np.zeros_like(splits_points)
        fset1[::2] = 1.0
        fset2 = np.zeros_like(splits_points)
        fset2[1::2] = 1.0
        plt.plot(splits_points, fset1, 'k')
        plt.plot(splits_points, fset2, 'k')
    else:
        plt.plot()

    plt.title('Feature %d' % subFeat)
    if subFeat % rows == 1:
        plt.ylabel('fSets')
    if subFeat > (rows * (cols - 1)):
        plt.xlabel('Feature value')
    plt.grid()
plt.show()