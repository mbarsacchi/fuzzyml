from __future__ import print_function

import numpy as np

from FuzzyML.datasets import load_iris
from FuzzyML.fbdt import fbdt
from FuzzyML.utils import pyTree

# Dataset loading
X, y = load_iris.load_iris()
n, m = X.shape
np.random.seed(0)
index_tr = np.random.choice(range(n), size=int(n * 0.7), replace=False)
index_te = np.setdiff1d(np.arange(0, n, dtype=int), index_tr)

Xtr = X[index_tr, :]
ytr = y[index_tr]
Xte = X[index_te, :]
yte = y[index_te]

# Inducing the tree
from FuzzyML.fmdt import  fmdt
myTree = fmdt.FMDT(max_depth=5, priorDiscretization=True, verbose=False,
                   ).fit(Xtr, ytr, [True, True, True, True])

print("Training on the IRIS dataset...")
print("Training accuracy: ",sum(myTree.predict(Xtr)==ytr)/float(len(ytr)))
print("Test accuracy: ",sum(myTree.predict(Xte)==yte)/float(len(yte)))
#pyTree.evalPyTree(myTree, 3)
