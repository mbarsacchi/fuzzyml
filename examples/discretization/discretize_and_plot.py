#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 20 17:26:25 2017

@author: marcobarsacchi
"""

from __future__ import division, print_function
import matplotlib.pyplot as plt
import numpy as np
from FuzzyML.discretization import discretizer_fuzzy as df
from os.path import dirname
from os.path import join
import csv


def load_dataset(name):
   
    parent_path = dirname(dirname(__file__))
   
    
    with open(join(parent_path, 'datasets', name+'.csv')) as csv_file:
        data_file = csv.reader(csv_file)
        temp = next(data_file)
        n_samples = int(temp[0])
        n_features = int(temp[1])
        data = np.empty((n_samples, n_features))
        target = np.empty((n_samples,), dtype=np.int)

        for i, ir in enumerate(data_file):
            data[i] = np.asarray(ir[:-1], dtype=np.float64)
            target[i] = np.asarray(ir[-1], dtype=np.int)

    return data, target

def triangularPlot (splits):
    
    plt.figure()
    rows = (len(splits)+1)//2
    cols = 2
    for subFeat in range(1, len(splits)+1):
        
        plt.subplot(rows, cols, subFeat)
        plot_points = splits[subFeat-1]
        if len(plot_points) != 0:
            fset1 = np.zeros_like(plot_points)
            fset1[::2] = 1.0
            fset2 = np.zeros_like(plot_points)
            fset2[1::2] = 1.0
            plt.plot(plot_points, fset1, 'k')
            plt.plot(plot_points, fset2, 'r')
        else:
            plt.plot()
    
        plt.title('Feature %d' % subFeat)
        if  subFeat==(rows*cols+1)//2:
            plt.ylabel('TriangularfSets')
        if subFeat > (cols * (rows - 1)):
            plt.xlabel('Feature value')
        plt.grid()
    
    plt.show()
    
    
    return 


def trapezoidalPlot (splits,trpzPrm=0.1):

    abcdPoints=[]
    for ifeat in range (0, len(splits)):        
        splitPoints=splits[ifeat]
        newpoints=[]
        if (len(splitPoints)>1 ):
            plateau=np.zeros(len(splitPoints)-1)       
            plateau=(splitPoints[1:]-splitPoints[:-1])*trpzPrm
            newpoints= np.zeros(2*len(splits[ifeat]))
            newpoints[0::2]= splitPoints[::]
            newpoints[1::2]= splitPoints[::]
            newpoints[1:-2:2]+=plateau[::]
            newpoints[2:-1:2]-=plateau[::]
        abcdPoints.append(newpoints)
        
    plt.figure()
    rows = (len(splits)+1)//2
    cols = 2
    
    for subFeat in range(1, len(abcdPoints) + 1):
        
        if ( len(abcdPoints)!=0 ):
            plt.subplot(rows, cols, subFeat)
            points = abcdPoints[subFeat-1]
        if len(points) != 0:
            
            fset1 = np.zeros_like(points)
            fset1[::4] = 1.0
            fset1[1::4] = 1.0
            
            fset2 = np.zeros_like(points)
            fset2[2::4] = 1.0
            fset2[3::4] = 1.0
            
            plt.plot(points, fset1, 'k')
            plt.plot(points, fset2, 'r')
        else:
            plt.plot()
    
        plt.title('Feature %d' % subFeat)
        if subFeat==(rows*cols+1)//2 == subFeat:
            plt.ylabel('fSetsTrapezoidal')
        if subFeat > (cols * (rows - 1)):
            plt.xlabel('Feature value')
        plt.grid()
    plt.show()


    return 
    
    
#def discretize_and_plot (
name='iris' 
contv=None, 
doTriangular=True 
doTrapezoidal=True
trpzPrm=0.4
                         
     
    # Load the dataset
Xtra, ytra = load_dataset(name)
    
    # Specify the continuos features 
contv = [True] * 4
print('here')
    # Discretize a dataset with Triangular Fuzzy
if doTriangular:
        fdiscr = df.FuzzyMDLFilter(3, Xtra, ytra, continous=contv, ftype="triangular")
        splits = fdiscr.run()  
        print("\n  \n *** SPLIT POINTS TRIANGOLARI *** \n")
        print(splits)
        print("\n")
        triangularPlot(splits)
        
    
    #Discretize a dataset with Trapezoidal Fuzzy
if doTrapezoidal:
        fdiscr = df.FuzzyMDLFilter(3, Xtra, ytra, continous=contv, ftype="trapezoidal", trpzPrm=trpzPrm)
        splits = fdiscr.run()
        print("\n  \n *** SPLIT POINTS TRAPEZOIDALI *** \n")
        print(splits)
        print("\n")
        trapezoidalPlot(splits,trpzPrm)