from __future__ import division, print_function

from time import time

import matplotlib.pyplot as plt
import numpy as np

from FuzzyML.datasets import load_iris
from FuzzyML.discretization import discretizer_fuzzy as df

# Load the dataset
Xtra, ytra = load_iris.load_iris()
# Specify the number of features
contv = [True] * 4
numFeat = len(contv)
rows = 2
cols = 2

t1 = time()

# Discretize a dataset
fdiscr = df.FuzzyMDLFilter(3, Xtra, ytra, continous=contv)
splits = fdiscr.run()

print("Time eval: %f" % (time() - t1))


plt.figure()

for subFeat in range(1, numFeat + 1):
    plt.subplot(rows, cols, subFeat)
    splits_points = splits[subFeat-1]
    if len(splits_points) != 0:
        fset1 = np.zeros_like(splits_points)
        fset1[::2] = 1.0
        fset2 = np.zeros_like(splits_points)
        fset2[1::2] = 1.0
        plt.plot(splits_points, fset1, 'k')
        plt.plot(splits_points, fset2, 'k')
    else:
        plt.plot()

    plt.title('Feature %d' % subFeat)
    if subFeat % rows == 1:
        plt.ylabel('fSets')
    if subFeat > (rows * (cols - 1)):
        plt.xlabel('Feature value')
    plt.grid()
plt.show()