FuzzyML package
===============

Subpackages
-----------

.. toctree::

    FuzzyML.adaboost
    FuzzyML.datasets
    FuzzyML.discretization
    FuzzyML.fbdt
    FuzzyML.fmdt
    FuzzyML.fuzzysets
    FuzzyML.utils

Module contents
---------------

.. automodule:: FuzzyML
    :members:
    :undoc-members:
    :show-inheritance:
