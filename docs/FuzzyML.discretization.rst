FuzzyML\.discretization package
===============================

Submodules
----------

FuzzyML\.discretization\.discretizer\_crisp module
--------------------------------------------------

.. automodule:: FuzzyML.discretization.discretizer_crisp
    :members:
    :undoc-members:
    :show-inheritance:

FuzzyML\.discretization\.discretizer\_fuzzy module
--------------------------------------------------

.. automodule:: FuzzyML.discretization.discretizer_fuzzy
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: FuzzyML.discretization
    :members:
    :undoc-members:
    :show-inheritance:
