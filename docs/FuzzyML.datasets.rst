FuzzyML\.datasets package
=========================

Submodules
----------

FuzzyML\.datasets\.load\_abalone module
---------------------------------------

.. automodule:: FuzzyML.datasets.load_abalone
    :members:
    :undoc-members:
    :show-inheritance:

FuzzyML\.datasets\.load\_concrete module
----------------------------------------

.. automodule:: FuzzyML.datasets.load_concrete
    :members:
    :undoc-members:
    :show-inheritance:

FuzzyML\.datasets\.load\_iris module
------------------------------------

.. automodule:: FuzzyML.datasets.load_iris
    :members:
    :undoc-members:
    :show-inheritance:

FuzzyML\.datasets\.load\_mortgage module
----------------------------------------

.. automodule:: FuzzyML.datasets.load_mortgage
    :members:
    :undoc-members:
    :show-inheritance:

FuzzyML\.datasets\.load\_stock module
-------------------------------------

.. automodule:: FuzzyML.datasets.load_stock
    :members:
    :undoc-members:
    :show-inheritance:

FuzzyML\.datasets\.load\_wankara module
---------------------------------------

.. automodule:: FuzzyML.datasets.load_wankara
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: FuzzyML.datasets
    :members:
    :undoc-members:
    :show-inheritance:
