#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 16 16:35:02 2017

@author: marcobarsacchi
"""

import numpy as np
from os.path import dirname
from os.path import join

def load_concrete():
    dataset_name = 'concrete'
    numCv = 5
    separator = ','
    data = []
    module_path = dirname(__file__)
    for fold in range(numCv):
        fopen = open(join(module_path,'data/concrete-5-fold/',dataset_name+'-'+str(numCv)+'-'+str(fold+1)+'tra.dat'),'r')
        loadTrain=[]
        for line in fopen:
            if not line.startswith('@'):
                try:
                    loadTrain.append([float(k) for k in line.replace('\n','').replace('\r','').split(separator)])
                except:
                    pass
        fopen.close()
        
        Xtra = np.array(loadTrain, dtype=float)[:,:-1]
        ytra = np.array(loadTrain, dtype=float)[:,-1]
        loadTest = []
        fopen = open(join(module_path,'data/concrete-5-fold/',dataset_name+'-'+str(numCv)+'-'+str(fold+1)+'tst.dat'),'r')
        for line in fopen:
            if not line.startswith('@'):
                loadTest.append(line.replace('\n','').replace('\r','').split(separator))
        fopen.close()
        Xtst = np.array(loadTest, dtype=float)[:,:-1]
        ytst = np.array(loadTest, dtype=float)[:,-1]
        data.append((Xtra,ytra,Xtst,ytst))
    return data