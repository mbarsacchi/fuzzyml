#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 22 14:15:17 2017

@author: marcobarsacchi
"""

import numpy as np


def tNorm(x, y, norm='product'):
    """
    Parameters
    ----------
    x : np.array
        Fist array of membership.
    y : np.array
        Second array of membership.
    norm : str
        Type of norm. Must be 'product', 'min', 'lukasiewicz' or 'hamacher'.

    Returns
    -------
    j : np.array
        Array of t-normed memberships
    """

    assert x.shape == y.shape
    # product t-norm
    if norm == 'product':
        return x * y
    # Minimum, or Godel t-norm
    elif norm == 'min':
        return np.minimum(x, y)
    # Lukasiewicz t-norm
    elif norm == 'lukasiewicz':
        return np.maximum(x + y - 1, np.zeros_like(x))
    # Hamacher t-norm
    elif norm == 'hamacher':
        return x * y / (x + y - x * y)
    else:
        raise Exception('Invalid norm')


def tConorm(x, y, norm='product'):
    """
    Parameters
    ----------
    x : np.array
        Fist array of membership.
    y : np.array
        Second array of membership.
    norm : str
        Type of conorm. Must be 'product', 'max', 'lukasiewicz' or 'hamacher'.

    Returns
    -------
    j : np.array
        Array of t-normed memberships
    """

    assert x.shape == y.shape
    # product s-norm
    if norm == 'product':
        return np.minimum(np.zeros_like(x), x + y - x * y)
    # Minimum, or Godel s-norm
    elif norm == 'min':
        return np.maximum(x, y)
    # Lukasiewicz s-norm
    elif norm == 'lukasiewicz':
        return np.minimum(np.ones_like(x), x + y)
    # Hamacher s-norm
    elif norm == 'hamacher':
        return (x + y - 2 * x * y) / (1 - x * y)
    else:
        raise Exception('Invalid norm')