import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle


def pythagoreanTree(H, S, ax, colors):
    """Pythagorean Tree.
    
    The tree model H, must provide:
        weight
        leftChild
        rightChild
        results [weighted samples for each class]
    
    Attributes
    ----------
    Hv: binary hierarchy
    S: representative square S = (c, ds, teta)
    c = (xc,yc): center
    ds: length of a side
    teta: slope angle
    
    """
    color = colors[np.argmax(H.results)]
    drawSquare(S, ax, color, H.results[np.argmax(H.results)])
    c, ds, teta = S
    if H.leftChild:
        w1 = H.leftChild.weight
        w2 = H.rightChild.weight
        alpha = np.pi / 2. * w2 / (w1 + w2)
        beta = np.pi / 2. * w1 / (w1 + w2)
        ds1 = ds * np.sin(beta)
        ds2 = ds * np.sin(alpha)
        c1 = computeCenter(S, ds1, alpha, left=True)
        c2 = computeCenter(S, ds2, beta, left=False)
        S1 = (c1, ds1, teta + alpha)
        S2 = (c2, ds2, teta - beta)
        pythagoreanTree(H.leftChild, S1, ax, colors)
        pythagoreanTree(H.rightChild, S2, ax, colors)


# def RotM(alpha):
#    """ Rotation Matrix for angle ``alpha`` """
#    sa, ca = np.sin(alpha), np.cos(alpha)
#    return np.array([[ca, -sa],
#                     [sa,  ca]])
#
# def getSquareVertices(mm, h, phi):
#    """ Calculate the for vertices for square with center ``mm``,
#        side length ``h`` and rotation ``phi`` """
#    hh0 = np.ones(2)*h  # initial corner
#    vv = [np.asarray(mm) + reduce(np.dot, [RotM(phi), RotM(np.pi/2*c), hh0])
#          for c in range(5)]  # rotate initial corner four times by 90 degrees
#    return np.asarray(vv)


def drawSquare(S, ax, color, hue):
    c, ds, teta = S
    d = np.sqrt(2) / 2. * ds
    #    if 0 <= teta < (np.pi/2.):
    #        vertex  = [c[0] + d*np.cos(5*np.pi/4.+teta),c[1] + d*np.sin(5*np.pi/4.+teta)]
    #    elif (np.pi/2.)<=teta<np.pi:
    #        vertex  = [c[0] + d*np.cos(3*np.pi/4.+teta),c[1] + d*np.sin(3*np.pi/4.+teta)]
    #    elif np.pi<=teta<(np.pi/3.*4):
    #        vertex  = [c[0] + d*np.cos(np.pi/4.+teta),c[1] + d*np.sin(np.pi/4.+teta)]
    #    else:
    #        vertex  = [c[0] + d*np.cos(-np.pi/4.+teta),c[1] + d*np.sin(-np.pi/4.+teta)]
    vertex = [c[0] + d * np.cos(5 * np.pi / 4. + teta), c[1] + d * np.sin(5 * np.pi / 4. + teta)]

    angle_plot = teta / 2 / np.pi * 360
    ax.add_patch(Rectangle(vertex, ds, ds, angle=angle_plot, fill=True, facecolor=color, alpha=hue, edgecolor='k'))
    # v = getSquareVertices(c,ds/2.,teta)
    # for k in range(len(v)-1):
    #    plt.plot(v[k],v[k+1],'k')


def computeCenter(S, length, alpha, left=True):
    parent_center, parent_length, parent_angle = S

    if left:
        d = np.sqrt(2) / 2. * parent_length
        vertex = [parent_center[0] + d * np.cos(3 * np.pi / 4. + parent_angle),
                  parent_center[1] + d * np.sin(3 * np.pi / 4. + parent_angle)]
        baseAngle = alpha + parent_angle
        d = np.sqrt(2) / 2. * length
        return [vertex[0] + d * np.cos(np.pi / 4. + baseAngle), vertex[1] + d * np.sin(np.pi / 4. + baseAngle)]
    else:
        d = np.sqrt(2) / 2. * parent_length
        vertex = [parent_center[0] + d * np.cos(np.pi / 4. + parent_angle),
                  parent_center[1] + d * np.sin(np.pi / 4. + parent_angle)]
        baseAngle = np.pi - alpha + parent_angle
        d = np.sqrt(2) / 2. * length
        return [vertex[0] + d * np.cos(-np.pi / 4. + baseAngle), vertex[1] + d * np.sin(-np.pi / 4. + baseAngle)]


def evalPyTree(model, numclasses, colors=None, show=True):
    """
    Parameters
    ----------
    model : a compatible tree model
        Tree model to plot. Currently only binary tree are supported.
    numclasses : int
        number of classes for the dataset.
    colors : np.array of colors, optional
        array of colors.
    Returns
    -------
        The axis.
    """
    plt.figure()
    ax = plt.subplot(111, aspect='equal')
    plt.grid('off')
    if not colors:
        cmap = plt.get_cmap('viridis')
        colors = cmap(np.linspace(0, 1, numclasses))
    pythagoreanTree(model.tree, ([0, 0], 1, 0), ax, colors)
    plt.autoscale()
    ax.axis('off')
    if show:
        plt.show()
    return ax