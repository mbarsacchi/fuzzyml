from __future__ import print_function

import logging
from unittest import TestCase

from FuzzyML.datasets import load_iris
from FuzzyML.discretization.discretizer_fuzzy import FuzzyMDLFilter


class TestFuzzyMDLFilter(TestCase):

    def test_discretization(self):
        l = logging.getLogger('FuzzyMDLFilter')
        l.setLevel(logging.INFO)

        Xtra, ytra = load_iris.load_iris()
        contv = [True] * 4

        fdiscr = FuzzyMDLFilter(3, Xtra, ytra, continous=contv, minGain=0.005, threshold=0, num_bins=500, ignore=True)
        splits = fdiscr.run()
        self.assertIsNotNone(self,splits)
        print("Fuzzy Discretization test OK\n")
