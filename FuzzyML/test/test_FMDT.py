from __future__ import print_function

from unittest import TestCase

import numpy as np

from FuzzyML.datasets import load_iris
from FuzzyML.fmdt import fmdt


class TestFBDT(TestCase):

    def test_fbdt(self):
        X, y = load_iris.load_iris()
        n, m = X.shape
        np.random.seed(0)
        index_tr = np.random.choice(range(n), size=int(n * 0.7), replace=False)
        index_te = np.setdiff1d(np.arange(0, n, dtype=int), index_tr)

        Xtr = X[index_tr, :]
        ytr = y[index_tr]
        Xte = X[index_te, :]
        yte = y[index_te]
        myTree = fmdt.FMDT(max_depth=5, priorDiscretization=True, verbose=False,
                           features='all').fit(Xtr, ytr, [True, True, True, True])
        self.assertEqual(sum(myTree.predict(Xte) == yte) / float(len(yte)),0.9555555555555556)
        print("Fuzzy Multi-way Decision Tree test OK\n")