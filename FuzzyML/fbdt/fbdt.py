#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 28 14:00:19 2016

@author: marcobarsacchi
"""
from __future__ import print_function
import os
import concurrent.futures as futures
import logging

import numpy as np
from numba import jit
import itertools

#
from numpy import linalg as la
#

from FuzzyML.discretization import discretizer_fuzzy as df


logger  = logging.getLogger('FuzzyFBDT')

class FuzzyImpurity(object):
    """Fuzzy impurity class.

    """

    @staticmethod
    def calculate(counts, totalCount):
        """Evaluate fuzzy impurity, given a list of fuzzy cardinalities.

        Parameters
        ----------
        counts : array-like
            An array, containing the fuzzy cardinality for a fuzzy set for
            each class.
        totalCount : float
            Global cardinality for a fuzzy set.

        Returns
        -------
        fuzzy_impurity : float
            Fuzzy impurity evaluated on the given set.
        """
        if totalCount == 0:
            return 0.0
        numClasses = len(counts)
        impurity = 0.0
        classIndex = 0

        while (classIndex < numClasses):
            classCount = counts[classIndex]
            if classCount != 0:
                freq = classCount / float(totalCount)
                impurity -= freq * np.log2(freq)
            classIndex += 1
        return impurity


# TNORM
def tNorm(x, y, tnorm='product'):
    """Method for calculating various elemntwise t_norms.

        Parameters
        ----------
        x : array-like
            First array
        y : array-like
            Second array
        tnorm : string, optional
            Type of T-Norm to be calculated. Can be either product or min
        Returns
        -------
        tnorm: array-like
            evaluated T-Norm
    """

    if tnorm == 'product':
        return np.multiply(x, y)
    if tnorm == 'min':
        return np.minimum(x, y)


@jit
def fSetMembership(obs, feat, val, isCont, left):
    """Fuzzy set membership function evaluator for two trapezoidal fuzzy sets.

    Parameters
    ----------
    obs, np.array, shape (1,Nfeatures)
        observation vector
    feat, int
        integer, containing the feature index for the membership evaluation.
    val, float
        current value, correspond to a fuzzy set, or to a category
    isCont, bool
        True if the feature is continous, False otherwise (return 1.0)
    left, Boolean
        determines whether we are on a left or on a right branch.

    Notes
    -----
    Assumes binary splitting:
    
    ::
    
        Left Branch:
    
             a
        ______
              -
               - _______
                 b
    
        Right Branch:
    
             a
                 _______
               -
              -
        ------
                 b
    
    Binary splitting ends up generating trapezoidal membership functions.
    """
    # root Node, or empty node
    if val is None or feat == -1 or left is None:
        return 1.0

    x = obs[feat]

    # Categorical feature, the membership is either 0 or 1
    if not isCont:
        if left:
            if x in val[0]:
                return 1.0
            else:
                return 0.0
        else:
            if x in val[1]:
                return 1.0
            else:
                return 0.0

    # Continous feature
    else:
        a = val[0]
        b = val[1]
        # Evaluating a left branch
        if left:

            if x > b:
                return 0.0
            elif x < a:
                return 1.
            else:
                return 1. - (x - a) / float(b - a)
        # Evaluating a left branch
        else:
            if x > b:
                return 1.0
            elif x < a:
                return 0.
            else:
                return (x - a) / float(b - a)


def findContinous(X):
    """
    Parameters
    ----------
    X, np.array, shape (Nsample,Nfeatures)

    Returns
    -------
    list, len Nfeatures
        A list containing True if the corresponding element is regarded as continous,
        False otherwise
    """
    N, M = X.shape
    red_axis = np.array([len(np.unique(X[:, k])) for k in range(M)]) / float(N)
    return list(red_axis > 0.01)


class decisionNode:
    def __init__(self, feature=-1, value=None, isLeaf=False, splits=None,
                 splitsHistory=None, results=None,
                 leftChild=None, rightChild=None, weight=None):
        # Feature corresponding to the set on the node.
        # it is not the feature used for splitting the child.
        self.feature = feature
        self.value = value
        # Currently not implemented
        # It will contain the splitting feature and the set associated to the node
        self.splits = splits
        self.isLeaf = isLeaf
        self.results = results
        self.leftChild = leftChild
        self.rightChild = rightChild
        self.weight = weight

    def predict(self, observation, cont, currMD, numClasses, left=None):
        """
        Decision Node prediction function.

        """
        if currMD > 0:
            if self.isLeaf:
                return self.results * tNorm(currMD,
                                            fSetMembership(observation, self.feature, self.value, cont[self.feature],
                                                           left))
            else:
                v = np.zeros(numClasses)
                v += self.leftChild.predict(observation, cont, tNorm(currMD, fSetMembership(observation, self.feature,
                                                                                            self.value,
                                                                                            cont[self.feature], left)),
                                            numClasses, left=True)
                v += self.rightChild.predict(observation, cont, tNorm(currMD, fSetMembership(observation, self.feature,
                                                                                             self.value,
                                                                                             cont[self.feature], left)),
                                             numClasses, left=False)
                return v
        else:
            return np.zeros(numClasses)

    def _ruleMine(self, continous, prec="", ruleArray=None):
        if ruleArray == None:
            ruleArray = []
        if self.isLeaf:
            prec += " then " + ("[" + ', '.join(['%.2f'] * len(self.results)) + "]") % tuple(self.results)
            ruleArray.append(prec.replace('\n', ''))
        else:
            if prec != "":
                prec += " and "
            else:
                prec += "if "
            if self.leftChild is None:
                pass
            else:
                if continous[self.leftChild.feature]:
                    precL = prec + "A" + str(self.leftChild.feature) + " is " + "lF - " + ("[" + ', '.join(
                        ['%.2f'] * len(self.leftChild.value)) + "]") % tuple(self.leftChild.value)
                    self.leftChild._ruleMine(continous, precL, ruleArray)
                else:

                    precL = prec + "A" + str(self.leftChild.feature) + " is " + "lVal == " + ("[" + ', '.join(
                        ['%.2f'] * len(self.leftChild.value[0])) + "]") % tuple(self.leftChild.value[0])
                    self.leftChild._ruleMine(continous, precL, ruleArray)
            if self.rightChild is None:
                pass
            else:
                if continous[self.rightChild.feature]:

                    precR = prec + "A" + str(self.rightChild.feature) + " is " + "rF - " + ("[" + ', '.join(
                        ['%.2f'] * len(self.rightChild.value)) + "]") % tuple(self.rightChild.value)
                    self.rightChild._ruleMine(continous, precR, ruleArray)
                else:
                    precR = prec + "A" + str(self.rightChild.feature) + " is " + "Val != " + ("[" + ', '.join(
                        ['%.2f'] * len(self.rightChild.value[1])) + "]") % tuple(self.rightChild.value[1])
                    self.rightChild._ruleMine(continous, precR, ruleArray)

        return ruleArray

    def _printSubTree(self, indentFactor=0, continous=None):
        """Print the sub-tree.
        """
        prefix = "|    " * indentFactor
        stringa = ""
        if self.isLeaf:
            stringa += " : " + ("[" + ', '.join(['%.2f'] * len(self.results)) + "]") % tuple(self.results)
        else:
            if self.leftChild is None:
                stringa += ""
            else:
                if continous[self.leftChild.feature]:
                    stringa += "\n" + prefix + "Feature: " + str(self.leftChild.feature) + " - lF - " + (
                                                                                                        "[" + ', '.join(
                                                                                                            [
                                                                                                                '%.2f'] * len(
                                                                                                                self.leftChild.value)) + "]") % tuple(
                        self.leftChild.value)
                    stringa += self.leftChild._printSubTree(indentFactor + 1, continous)
                else:
                    stringa += "\n" + prefix + "Feature: " + str(self.leftChild.feature) + " - lVal in " + (
                                                                                                           "[" + ', '.join(
                                                                                                               [
                                                                                                                   '%.2f'] * len(
                                                                                                                self.leftChild.value[0])) + "]") % tuple(
                        self.leftChild.value[0])
                    stringa += self.leftChild._printSubTree(indentFactor + 1, continous)
            if self.rightChild is None:
                stringa += ""
            else:
                if continous[self.leftChild.feature]:

                    stringa += "\n" + prefix + "Feature: " + str(self.rightChild.feature) + " - rF - " + (
                                                                                                         "[" + ', '.join(
                                                                                                             [
                                                                                                                 '%.2f'] * len(
                                                                                                                 self.rightChild.value)) + "]") % tuple(
                        self.rightChild.value)
                    stringa += self.rightChild._printSubTree(indentFactor + 1, continous)
                else:
                    stringa += "\n" + prefix + "Feature: " + str(self.rightChild.feature) + " - rVal in " + (
                                                                                                           "[" + ', '.join(
                                                                                                               [
                                                                                                                   '%.2f'] * len(
                                                                                                                 self.rightChild.value[1])) + "]") % tuple(
                        self.rightChild.value[1])
                    stringa += self.rightChild._printSubTree(indentFactor + 1, continous)

        return stringa

    def _printSubTreeWeights(self, indentFactor=0, continous=None):
        """Return the sub-tree weights.
        """
        if self.leftChild is None and self.rightChild is None:
            return self.weight
        else:
            return [self.weight, self.leftChild._printSubTreeWeights(), self.rightChild._printSubTreeWeights()]

    def _sons(self):
        array = []
        if self.leftChild:
            array.append(self.leftChild)
        if self.rightChild:
            array.append(self.rightChild)
        return array

    def _numDescendants(self):
        if self.isLeaf:
            return 1
        else:
            return 1 + self.leftChild._numDescendants() + self.rightChild._numDescendants()

    def _num_leaves(self):
        """Count number of non empty leaves.
        """
        if self.isLeaf == 1 and np.sum(self.results) != 0:
            leaf = 1.
        else:
            leaf = 0.
        if not self.leftChild is None:
            leaf += self.leftChild._num_leaves()
        if not self.rightChild is None:
            leaf += self.rightChild._num_leaves()
        return leaf


class FBDT(object):
    """Class implementing a binary fuzzy decision tree.

    """

    def __init__(self, max_depth=15, discr_minImpurity=0.02,
                 discr_minGain=0.01, discr_threshold=0, minGain=0.01,
                 minNumExamples=2, max_prop=1.0, extremely_random=False,
                 features='all',
                 priorDiscretization=True, seed=None,cat_type='one-vs-all',
                 verbose=False):
        """FBDT with fuzzy discretization on the node.

        Parameters
        ----------
        max_depth : int, optional, default = 15
            maximum tree depth.
        discr_minImpurity : int, optional default = 0.02
            minimum imputiry for a fuzzy set during discretization
        discr_minGain : float, optional, default = 0.01
            minimum entropy gain during discretization
        discr_threshold : int, optional, default = 0
            if discr_threshold != 0  the discretization is stopped
            at n = discr_threshold + 2 fuzzy sets.
        minGain : float, optional, default = 0.01
            minimum entropy gain for a split during tree induction
        minNumExample : int, optional, default = 2
            minimum number of example for a node during tree induction
        max_prop : float, optional, default = 1.0
            min proportion of samples pertaining to the same class in the same node
            for stop splitting.
        prior_discretization : boolean, optional, default = True
            define whether performing prior or node level discretization

        Notes
        -----
        When using the  FBDT in a fuzzy random forest implementation, programmers
        are encouraged to use priorDiscretization = True and providing the cPoints
        upon fit. This would help in speeding up computation.
        """

        self.max_depth = max_depth
        self.verbose = verbose
        self.discr_minImpurity = discr_minImpurity
        self.discr_minGain = discr_minGain
        self.minGain = minGain
        self.minNumExamples = minNumExamples
        self.max_prop = max_prop
        self.features = features
        self.discr_threshold = discr_threshold
        self.priorDiscretization = priorDiscretization
        self.extremely_random = extremely_random
        self.seed = seed
        if self.seed is not None:
            self.seedCount = 1
        assert features in ['all', 'sqrt', 'log2','onethird'], "features must be in [all', 'sqrt', 'log2', 'onethird']"
        assert cat_type in ['one-vs-all','impurity','all'], "cat_type must be in ['one-vs-all','impurity','all']"
        self.cat_type = cat_type
        self.max_arity = 6

    def fit(self, X, y, continous=None, numClasses=None, cPoints=None):
        """Build a fuzzy binary decision tree classifier from the training set (X, y).

        Parameters
        ----------
        X : array-like or sparse matrix, shape = [n_samples, n_features]
            The training input samples.

        y : array-like, shape = [n_samples] or [n_samples, n_outputs]
            The target values (class labels) as integers.

        continous: array or list like
            Array or list of boolean, specifying if each feature is
            continous or categorical. Please provide this information for better
            evaluation.

        numClasses : int
            Number of classes for the dataset.

        cPoints: np.array of np.array of float, optional
            Split points.
        Returns
        -------
        self : object
            Returns self.
        """

        if not numClasses is None:
            self.K = numClasses
        else:
            self.K = int(np.max(y) + 1)

        if continous is None:
            self.cont = np.array(findContinous(X))
        else:
            self.cont = np.array(continous)

        self.N, self.M = X.shape

        self.numFeatures = self.M


        self.cPoints = None
        self.feature_indices = range(self.M)
        if self.priorDiscretization:
            if cPoints is None:
                discr = df.FuzzyMDLFilter(self.K, X, y, list(self.cont),
                                          minGain=self.discr_minGain,
                                          minImpurity=self.discr_minImpurity,
                                          threshold=self.discr_threshold)
                self.cPoints = np.array(discr.run())
            else:
                self.cPoints = cPoints
            features = [indk for indk,k in enumerate(self.cPoints) if len(k)>0]
            self.M = len(features)
            self.feature_indices = features

        if self.features == 'all':
            pass
        elif self.features == 'sqrt':
            self.numFeatures = np.sqrt(self.M)
        elif self.features == 'log2':
            self.numFeatures = np.log2(self.M)
        elif self.features == 'onethird':
            self.numFeatures = float(self.M)/3.

        tree = self.buildtree(np.concatenate((X, y.reshape(y.shape[0], 1)), axis=1), depth=0)
        self.tree = tree
        return self

    def buildtree(self, rows, scoref=FuzzyImpurity, depth=0,
                  memb_degree=None, attributes=None, val=None, feature=-1):

        if attributes == None:
            attributes = self.feature_indices

        # Implements Random Feature Selection at node level
        if self.features != 'all':
            if self.seed is not None:
                np.random.seed(self.seed * self.seedCount)
                self.seedCount += 1
            attributes = list(np.random.choice(attributes, size=int(self.numFeatures), replace=False))
        # Membership degree of the given set of samples for the current node
        if memb_degree is None:
            memb_degree = np.ones(len(rows))

        # Set up some variables to track the best criteria
        best_gain = 0.0
        best_criteria = None
        best_sets = None

        class_counts = self.classCounts(rows, memb_degree)

        # If one of the following occurs return a Leaf:
        # - Class counts for one of the classes is higher then a threshold
        # - numExamples < minNumExamples
        # - empty attributes list (only for multisplit trees)
        with np.errstate(divide='ignore', invalid='ignore'):
            if (np.any(class_counts / float(np.sum(class_counts)) >= self.max_prop) or
                        np.sum(class_counts) < self.minNumExamples or not attributes):

                return decisionNode(results=np.nan_to_num(class_counts / float(np.sum(class_counts))), feature=feature,
                                    isLeaf=True, value=val, weight=np.sum(memb_degree))

        # Pre-split entropy
        current_score = scoref.calculate(class_counts, sum(class_counts))

        # Iterate among features
        gain = best_gain
        for col in attributes:

            # Is the feature continous
            if self.cont[col]:

                if self.priorDiscretization:
                    split_values = self.cPoints[col]
                else:
                    split_values = \
                    df.FuzzyMDLFilter(self.K, rows[:, col:col + 1], rows[:, -1], list(self.cont)[col:col + 1],
                                      minGain=self.discr_minGain,
                                      minImpurity=self.discr_minImpurity,
                                      threshold=self.discr_threshold).run()[0]

                if len(split_values) > 2:

                    # Extremely randomized trees, evaluated only a fixed number of
                    # randomly selected splits
                    if self.extremely_random:
                        selected = np.random.choice(split_values[1:-1], min(self.numFeatures, len(split_values) - 2),
                                                    replace=False)
                        split_values = np.concatenate((split_values[0:1], np.sort(selected), split_values[-1:]))

                        # End of extremely randomized tree part
                    for k in range(len(split_values) - 1):
                        splits = split_values[k:k + 2]
                        row_vect, memb_vect, values = self.__binarydivideset(rows, memb_degree, col, splits)

                        classes = np.array([np.sum(z) for z in memb_vect])
                        pj = classes / np.sum(classes)
                        cCounts = [self.classCounts(r, m) for r, m in zip(row_vect, memb_vect)]
                        I = np.array([scoref.calculate(c, sum(c)) for c in cCounts])

                        # Eval Gain
                        gain = current_score - np.sum(pj * I)
                        if gain > best_gain:
                            best_gain = gain
                            best_criteria = col
                            best_sets = (memb_vect, row_vect, values)
            
            # Categorical feature
            else:
                split_values = self.__sort_categorical(rows, memb_degree, col, self.K, scoref)
                for cat in split_values:
                    row_vect, memb_vect, values = self.__catdivideset_group(rows, memb_degree, col, cat)
                    classes = np.array([np.sum(k) for k in memb_vect])
                    pj = classes / np.sum(classes)
                    cCounts = [self.classCounts(r, m) for r, m in zip(row_vect, memb_vect)]
                    I = np.array([scoref.calculate(c, sum(c)) for c in cCounts])
                    # Eval Gain
                    gain = current_score - np.sum(pj * I)
                    if gain > best_gain:
                        best_gain = gain
                        best_criteria = col
                        best_sets = (memb_vect, row_vect, values)
        
        if best_gain > self.minGain and depth < self.max_depth:
            memb_vect, row_vect, values = best_sets
            retCounts = self.classCounts(rows, memb_degree)
            leftChild = self.buildtree(rows=row_vect[0], memb_degree=memb_vect[0], depth=depth + 1, val=values,
                                       feature=best_criteria)
            rightChild = self.buildtree(rows=row_vect[1], memb_degree=memb_vect[1], depth=depth + 1, val=values,
                                        feature=best_criteria)
            return decisionNode(results=np.nan_to_num(retCounts / float(np.sum(retCounts))), feature=feature,
                                leftChild=leftChild, rightChild=rightChild, value=val, weight=np.sum(memb_degree))

        else:
            return decisionNode(results=np.nan_to_num(class_counts / float(np.sum(class_counts))), feature=feature,
                                isLeaf=True, value=val, weight=np.sum(memb_degree))

    def __catdivideset(self, rows, membership, column):
        """Splitter for categorical features.
        """
        uniqueset = list(set(rows[:, column]))
        uniqueset.sort()
        memb_vect = []
        row_vect = []
        for k in uniqueset:
            split_function = lambda data: data[:, column] == k
            index = split_function(rows)
            memb_vect.append(membership[index])
            row_vect.append(rows[index, :])

        return row_vect, memb_vect, uniqueset
    
    def __catdivideset_group(self, rows, membership, column, cat):
        """Binary splitter for categorical features.
        Given a set of rows, the corresponding membership values, the column to split
        and the current categorical value, generates two sets, where
        Left Branch:
            feat[k] == cat
        Right Branch:
            feat[k] != cat
        """

        memb_vect = []
        row_vect = []
        split_function = lambda data: map(lambda x: x in cat[0],data[:, column])
        index = list(split_function(rows))
        index_not =list(map(lambda x: not x, index))
        memb_vect.append(membership[index])
        row_vect.append(rows[index, :])
        memb_vect.append(membership[index_not])
        row_vect.append(rows[index_not, :])
        return row_vect, memb_vect, cat

    def __binarydivideset(self, rows, membership, column, discr):
        """Binary splitter for continous fuzzy features.
        Given a set of rows, the corresponding membership values, the column to split
        and the current discretization split, defined by two points [a, b], generates two sets, where

        Left Branch:

             a
        ______
              -
               - _______
                 b

        Right Branch:

             a
                 _______
               -
              -
        ------
                 b

        """

        memb_vect = []
        row_vect = []

        split_function = lambda data: data[:, column] <= discr[1]
        index = split_function(rows)
        row_vect.append(rows[index, :])
        curr_memb = FBDT.tNorm(FBDT.fuzzy_membership(rows[index, :], column, discr, left=True),
                                     membership[index])
        memb_vect.append(curr_memb)

        split_function = lambda data: data[:, column] >= discr[0]
        index = split_function(rows)
        row_vect.append(rows[index, :])
        curr_memb = FBDT.tNorm(FBDT.fuzzy_membership(rows[index, :], column, discr, left=False),
                                     membership[index])
        memb_vect.append(curr_memb)

        return row_vect, memb_vect, discr

    @staticmethod
    def fuzzy_membership(rows, column, points, left=True):

        x = rows[:, column]
        memb = np.zeros_like(x)
        a = points[0]
        b = points[1]

        # Left membership
        if left:
            for k in range(len(x)):
                if x[k] <= a:
                    memb[k] = 1.
                elif x[k] >= b:
                    memb[k] = 0.
                else:
                    memb[k] = 1 - (x[k] - a) / float(b - a)
        # Right membership
        else:
            for k in range(len(x)):
                if x[k] <= a:
                    memb[k] = 0.
                elif x[k] >= b:
                    memb[k] = 1.
                else:
                    memb[k] = (x[k] - a) / float(b - a)
        return memb

    def classCounts(self, rows, memb_degree):
        labels = rows[:, -1]
        numClasses = self.K
        llab = list(set(labels))
        llab.sort()
        counts = np.zeros(numClasses, dtype=float)
        for k in range(len(llab)):
            ind = int(llab[k])
            counts[ind] = np.sum(memb_degree[labels == llab[k]])
        return counts
    
    def __sort_categorical(self, rows, memb_degree, feat, numClasses, scoref, approx=True):
        cats = []
        uniqueset = np.unique(rows[:, feat])
        uniqueset.sort()
        
        if self.cat_type == 'all':
            if len(uniqueset)>1 and len(uniqueset) < self.max_arity :    
                cats = map(lambda x:(list(x[0]),list(x[1])),binary_splits_no_dupes(uniqueset))
            elif len(uniqueset)>1:    
                for k in uniqueset:
                    cats.append(([k],list(set(uniqueset)-set([k]))))
            return cats
        
        elif self.cat_type == 'one-vs-all':

            if len(uniqueset)>1:    
                for k in uniqueset:
                    cats.append(([k],list(set(uniqueset)-set([k]))))
            return cats
        
        elif self.cat_type == 'impurity':
            if numClasses == 2:
                row_vect, _, _ = self.__catdivideset(rows, memb_degree, feat)
                classFractions = [sum(values[:,-1])/len(values) for values in row_vect]
                values = uniqueset[np.argsort(classFractions)]
                for k in range(1,len(values)):
                    cats.append((values[:k], values[k:]))
                return cats
            else:
                row_vect, memb_vect, _ = self.__catdivideset(rows, memb_degree, feat)
                cCounts = [self.classCounts(r, m) for r, m in zip(row_vect, memb_vect)]
                if len(uniqueset)<=2 or approx:    
                    I = np.array([scoref.calculate(c, sum(c)) for c in cCounts])
                    values = uniqueset[np.argsort(I)]
                    for k in range(1,len(values)):
                        cats.append((values[:k], values[k:]))
                    return cats
                else:
                    pMatrix = np.array(cCounts).T
                    pMatrix -= np.mean(pMatrix, axis=0)
                    C = np.cov(pMatrix, rowvar=False)
                    l, principal_axes = la.eig(C)
                    values = uniqueset[np.argsort(l)]
                    for k in range(1,len(values)):
                        cats.append((values[:k], values[k:]))
                    return cats 
        else:
            raise Exception('Invalid categorical split selection method')
                
            


    def predict(self, X, multiprocessing = False):
        """Predict class or regression value for X.

        For a classification model, the predicted class for each sample in X is
        returned. For a regression model, the predicted value based on X is
        returned.

        Parameters
        ----------
        X : array-like  of shape = [n_samples, n_features]
            The input samples.

        Returns
        -------
        y : array of shape = [n_samples] or [n_samples, n_outputs]
            The predicted classes, or the predict values.
        """
        """ MULTIPROCESSING
        
        """

        if multiprocessing:
            try:
                num_cores = os.cpu_count()
            except:
                num_cores = 4
            num_cores = min(num_cores, X.shape[0])

            indexes = np.linspace(0,X.shape[0],num_cores+1,dtype=int)

            with futures.ProcessPoolExecutor() as executor:
                to_do = []
                for k in range(len(indexes) - 1):
                    future = executor.submit(FBDT._classify,*(self,X[indexes[k]:indexes[k+1],:],k))
                    to_do.append(future)
                    msg = 'Scheduled for {}: {}'
                    logger.info(msg.format(k, future))
                results = {}
                for future in futures.as_completed(to_do):
                    res = future.result()
                    msg = '{} result: {!r}'
                    logger.info(msg.format(future, res))
                    results[res[0]] = res[1]

                pvect = np.empty((0))
                for key in sorted(list(results.keys())):
                    pvect = np.concatenate((pvect,results[key]))

        else:
            return np.array(list(map(lambda X: FBDT._classify(X, self.tree, self.cont, self.K), X)))

    def predictRF(self, obs):
        """Prediction for RF.
        """
        prediction = self.tree.predict(obs, self.cont, 1., self.K)
        return prediction

    def printTree(self):

        stringTree = "Fuzzy Decision tree \n" + self.tree._printSubTree(0, self.cont)

        print(stringTree)

    def numNodes(self):

        return np.sum(list(map(lambda x: x._numDescendants(), self.tree._sons())))

    def printTreeWeights(self):
        return self.tree._printSubTreeWeights(0, self.cont)

    def numLeaves(self):
        return self.tree._num_leaves()

    @staticmethod
    def _classify(observation, tree, cont, numClass):
        prediction = tree.predict(observation, cont, 1., numClass)
        return np.argmax(prediction)

    @staticmethod
    def tNorm(array1, array2, tnorm='product'):
        """Method for calculating various elemntwise t_norms.

        Parameters
        ----------
        array1, numpyp.array()
            First array
        array2, numpy.array()
            Second array
        """
        if array1.shape != array2.shape:
            raise Exception('Array of wrong size')

        if tnorm == 'product':
            return array1 * array2
        if tnorm == 'min':
            return np.minimum(array1, array2)


def binary_splits(seq):
    
    for result_indices in itertools.product((0,1), repeat=len(seq)):
        result = ([], [])
        for seq_index, result_index in enumerate(result_indices):
            result[result_index].append(seq[seq_index])
        #skip results where one of the sides is empty
        if not result[0] or not result[1]: continue
        #convert from list to tuple so we can hash it later
        yield map(tuple, result)

def binary_splits_no_dupes(seq):
    seen = set()
    for item in binary_splits(seq):
        key = tuple(sorted(item))
        if key in seen: continue
        yield key
        seen.add(key)