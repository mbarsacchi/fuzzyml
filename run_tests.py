#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun 16 14:09:33 2017

@author: marcobarsacchi
"""
import unittest

loader = unittest.TestLoader()
start_dir = '/Users/marcobarsacchi/PycharmProjects/FuzzyML/FuzzyML/test'
suite = loader.discover(start_dir)

runner = unittest.TextTestRunner()
runner.run(suite)
